package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.EquivalenciaIngredientePedido;

public class DAOTablaEquivalenciaIngredientePedido {
	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTablaEquivalenciaIngredientePedido() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	public ArrayList<EquivalenciaIngredientePedido> darEquivalenciasIngredientePorRestaurante(String nomR) throws Exception, SQLException
	{
		ArrayList<EquivalenciaIngredientePedido> prodPed = new ArrayList<EquivalenciaIngredientePedido>();

		String sql = "SELECT * FROM EquivalenciaIngredientePedido";
		sql += " WHERE NOMRESTAURANTE = '" + nomR+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			String nomPz=rs.getString("NOMPRODUCTO");
			String nomPr=rs.getString("NOMINGREDIENTE");
			String nomRe=rs.getString("NOMRESTAURANTE");
			String nomE=rs.getString("NOMEQUIVALENCIA");
			prodPed.add(new EquivalenciaIngredientePedido(idp, nomPz, nomRe,nomE, nomPr));
		}
		return prodPed;
	}	
	
	public ArrayList<EquivalenciaIngredientePedido> darEquivalenciasIngredientePorIngrediente(String nomI) throws Exception, SQLException
	{
		ArrayList<EquivalenciaIngredientePedido> prodPed = new ArrayList<EquivalenciaIngredientePedido>();

		String sql = "SELECT * FROM EquivalenciaIngredientePedido";
		sql += " WHERE NOMINGREDIENTE = '" + nomI+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			String nomPz=rs.getString("NOMPRODUCTO");
			String nomPr=rs.getString("NOMINGREDIENTE");
			String nomRe=rs.getString("NOMRESTAURANTE");
			String nomE=rs.getString("NOMEQUIVALENCIA");
			prodPed.add(new EquivalenciaIngredientePedido(idp, nomPz, nomRe,nomE, nomPr));
		}
		return prodPed;
	}
	
	public ArrayList<EquivalenciaIngredientePedido> darEquivalenciasIngredientePorPedido(Long idP) throws Exception, SQLException
	{
		ArrayList<EquivalenciaIngredientePedido> prodPed = new ArrayList<EquivalenciaIngredientePedido>();

		String sql = "SELECT * FROM EquivalenciaIngredientePedido";
		sql += " WHERE IDPEDIDO = " + idP+"";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			String nomPz=rs.getString("NOMPRODUCTO");
			String nomPr=rs.getString("NOMINGREDIENTE");
			String nomRe=rs.getString("NOMRESTAURANTE");
			String nomE=rs.getString("NOMEQUIVALENCIA");
			prodPed.add(new EquivalenciaIngredientePedido(idp, nomPz, nomRe,nomE, nomPr));
		}
		return prodPed;
	}
	
	
	public void addEquivalenciaIngredientePedido(EquivalenciaIngredientePedido rest) throws SQLException, Exception {		
		
		
		String sql = "INSERT INTO EquivalenciaIngredientePedido VALUES (";
		sql += rest.getIdPedido()+ ",'";
		sql += rest.getNomProducto()+ "','";
		sql += rest.getNomIngrediente() + "','";
		sql += rest.getNomRestaurante() + "','";
		sql += rest.getNomEquivalencia() + "')";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();

	}
	
	
	public void deleteEquivalenciaIngredientePedido(EquivalenciaIngredientePedido rest) throws SQLException, Exception {

		String sql = "DELETE FROM EquivalenciaIngredientePedido";
		sql += " WHERE NOMRESTAURANTE = '" + rest.getNomRestaurante()+"'";
		sql += " AND NOMIngrediente = '" + rest.getNomIngrediente()+"'";
		sql += " AND NOMEquivalencia = '"+rest.getNomEquivalencia()+"'";
		sql += " AND NOMRESTAURANTE = '" + rest.getNomProducto()+"'";
		sql += " AND IDPEDIDO = "+rest.getIdPedido();

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}

}
