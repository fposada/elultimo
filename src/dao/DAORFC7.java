package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.RFC7;

public class DAORFC7 {

	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAORFC7() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	
	public ArrayList<RFC7> getPedidosUsuariosConsolidados() throws SQLException, Exception{
		ArrayList<RFC7> listaPedidos = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.IDUSUARIO, C.NUMMESA, C.NOMPRODUCTO, C.NOMRESTAURANTE, D.NOMMENU ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.IDUSUARIO, A.NUMMESA, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C LEFT OUTER JOIN PRODUCTOMENU D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String idUsuario = rs.getString("IDUSUARIO");
			Long numMesa = rs.getLong("NUMMESA");
			String nomProducto = rs.getString("NOMPRODUCTO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			String nomMenu = rs.getString("NOMMENU");
			RFC7 nuevo = new RFC7(idPedido, idUsuario, numMesa, nomProducto, nomRestaurante, nomMenu);
			listaPedidos.add(nuevo);
		}
		
		return listaPedidos;
		
	}
	
	public ArrayList<RFC7> getPedidosUsuarioConsolidado(String idDelUsuario) throws SQLException, Exception{
		ArrayList<RFC7> listaPedidos = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.IDUSUARIO, C.NUMMESA, C.NOMPRODUCTO, C.NOMRESTAURANTE, D.NOMMENU ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.IDUSUARIO, A.NUMMESA, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C LEFT OUTER JOIN PRODUCTOMENU D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO ";
		sql += "WHERE C.IDUSUARIO LIKE '" + idDelUsuario + "'";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String idUsuario = rs.getString("IDUSUARIO");
			Long numMesa = rs.getLong("NUMMESA");
			String nomProducto = rs.getString("NOMPRODUCTO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			String nomMenu = rs.getString("NOMMENU");
			RFC7 nuevo = new RFC7(idPedido, idUsuario, numMesa, nomProducto, nomRestaurante, nomMenu);
			listaPedidos.add(nuevo);
		}
		
		return listaPedidos;
		
	}
	
}
