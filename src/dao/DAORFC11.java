package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import vos.RFC11FechaProducto;
import vos.RFC11FechaRestaurante;

public class DAORFC11 {
	
	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAORFC11() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	//-----------------------------------------------------------------------------------------
	// PRODUCTO
	//-----------------------------------------------------------------------------------------
	
	public ArrayList<RFC11FechaProducto> darProductosMASConsumidos(String dia) throws SQLException, ParseException{
		ArrayList<RFC11FechaProducto> respuesta = new ArrayList<>();
		
		String maniana = diaSiguiente(dia);
				
		String sql = "SELECT COUNT(IDPEDIDO) AS CANTIDAD, NOMPRODUCTO ";
		sql += "FROM( ";
		sql += "SELECT PEDIDO.IDPEDIDO, PEDIDO.FECHA, PP.NOMPRODUCTO, PP.NOMRESTAURANTE ";
		sql += "FROM PRODUCTOPEDIDO PP INNER JOIN PEDIDO ";
		sql += "ON PP.IDPEDIDO = PEDIDO.IDPEDIDO ";
		sql += "WHERE PEDIDO.FECHA > TO_DATE('" + dia + "','DD-MM-YYYY') AND PEDIDO.FECHA";
		sql += " < TO_DATE('" + maniana + "','DD-MM-YYYY')) ";
		sql += "GROUP BY NOMPRODUCTO";
		
		System.out.println(sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			int cantidad = rs.getInt("CANTIDAD");
			String nomProducto = rs.getString("NOMPRODUCTO");
			String fecha = dia;
			RFC11FechaProducto nuevo = new RFC11FechaProducto(fecha,cantidad, nomProducto);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	//-----------------------------------------------------------------------------------------
	// RESTAURANTE
	//-----------------------------------------------------------------------------------------
	
	public ArrayList<RFC11FechaRestaurante> darRestaurantesMASFrecuentados(String dia) throws SQLException, ParseException{
		ArrayList<RFC11FechaRestaurante> respuesta = new ArrayList<>();
		
		String maniana = diaSiguiente(dia);
				
		String sql = "SELECT COUNT(IDPEDIDO) AS CANTIDAD, NOMRESTAURANTE ";
		sql += "FROM( ";
		sql += "SELECT PEDIDO.IDPEDIDO, PEDIDO.FECHA, PP.NOMPRODUCTO, PP.NOMRESTAURANTE ";
		sql += "FROM PRODUCTOPEDIDO PP INNER JOIN PEDIDO ";
		sql += "ON PP.IDPEDIDO = PEDIDO.IDPEDIDO ";
		sql += "WHERE PEDIDO.FECHA > TO_DATE('" + dia + "','DD-MM-YYYY') AND PEDIDO.FECHA";
		sql += " < TO_DATE('" + maniana + "','DD-MM-YYYY')) ";
		sql += "GROUP BY NOMRESTAURANTE";
		
		System.out.println(sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			int cantidad = rs.getInt("CANTIDAD");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			String fecha = dia;
			RFC11FechaRestaurante nuevo = new RFC11FechaRestaurante(fecha,cantidad, nomRestaurante);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	//-----------------------------------------------------------------------------------------
	// M�TODOS PRIVADOS
	//-----------------------------------------------------------------------------------------
	
	private String diaSiguiente(String dia) throws ParseException{
		Calendar calHoy = Calendar.getInstance();
		
		String pattern = "dd-mm-yyyy";
		Date dateHoy = new SimpleDateFormat(pattern).parse(dia);
		calHoy.setTime(dateHoy);
		
		calHoy.add(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(calHoy.getTime());
		
	}
}