package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.Solicitud;

public class DAOTablaSolicitudes {
	
	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaSolicitudes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTablaSolicitudes() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	
	public ArrayList<Solicitud> darSolicitudes() throws SQLException, Exception {
		ArrayList<Solicitud> Solicitudez = new ArrayList<Solicitud>();

		String sql = "SELECT * FROM SOLICITUD";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();
		

		while (rs.next()) {
			String idU=rs.getString("IDUSUARIO");
			String est=rs.getString("ESTADO");
			int idP=rs.getInt("IDPEDIDO");
			Solicitudez.add(new Solicitud(idU, est, idP));
		}
		return Solicitudez;
	}	
	
	public Solicitud buscarSolicitudPorIds(String idUs, int idP) throws SQLException, Exception 
	{
		Solicitud uzer = null;

		String sql = "SELECT * FROM SOLICITUD WHERE IDUSUARIO = '" + idUs+"' AND IDPEDIDO = "+idP;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if(rs.next()) {
			String idU=rs.getString("IDUSUARIO");
			String est=rs.getString("ESTADO");
			int idPz=rs.getInt("IDPEDIDO");
			uzer= new Solicitud(idU, est, idPz);
		}

		return uzer;
	}	
	
	
	
	

	public void addSolicitud(Solicitud us) throws SQLException, Exception {
		
		Solicitud testeo=buscarSolicitudPorIds(us.getIdUsuario(),us.getIdPedido());
		if(testeo!=null)
			throw new Exception("Un Solicitud con los datos de entrada ya existe.");

		String sql = "INSERT INTO SOLICITUD VALUES ('";
		sql += us.getIdUsuario() + "','";
		sql += us.getEstado() + "',";
		sql += us.getIdPedido() + ")";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
		

	}
	
	public void updateSolicitud(Solicitud us) throws SQLException, Exception {

		String sql = "UPDATE SOLICITUD SET ESTADO='";
		sql += us.getEstado() + "'";
		sql += " WHERE IDUSUARIO = '" + us.getIdUsuario()+"' AND IDPEDIDO = "+us.getIdPedido();


		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}
	
	public void deleteSolicitud(Solicitud us) throws SQLException, Exception {

		String sql = "DELETE FROM SOLICITUD";
		sql += " WHERE IDUSUARIO = '" + us.getIdUsuario()+"' AND IDPEDIDO = "+us.getIdPedido();

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}


}
