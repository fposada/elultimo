package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.RFC8NoRegistrado;
import vos.RFC8Registrado;

public class DAORFC8 {

	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAORFC8() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	public ArrayList<RFC8Registrado> darConsolidadoUsuariosRegistrados() throws SQLException, Exception{
		ArrayList<RFC8Registrado> lista = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.NOMRESTAURANTE, C.IDZONA, C.NUMMESA, C.IDUSUARIO, C.NOMPRODUCTO, D.COSTOPROD, D.PRECIO ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.SERVIDO, A.IDZONA, A.NUMMESA, A.IDUSUARIO, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C INNER JOIN RESTAURANTEPRODUCTO D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO ";
		sql += "WHERE C.SERVIDO = '1' AND IDUSUARIO IS NOT NULL";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			Long idZona = rs.getLong("IDZONA");
			Long numMesa = rs.getLong("NUMMESA");
			String idUsuario = rs.getString("IDUSUARIO");
			String nomProducto = rs.getString("NOMPRODUCTO");
			int costoProd = rs.getInt("COSTOPROD");
			int precio = rs.getInt("PRECIO");
			RFC8Registrado nuevo = new RFC8Registrado(idPedido, nomRestaurante, idZona, numMesa, idUsuario, nomProducto, costoProd, precio);
			lista.add(nuevo);
		}
		return lista;
	}
	
	
	public ArrayList<RFC8Registrado> darConsolidadoUsuariosRegistradosRestaurante(String nRestaurante) throws SQLException, Exception{
		ArrayList<RFC8Registrado> lista = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.NOMRESTAURANTE, C.IDZONA, C.NUMMESA, C.IDUSUARIO, C.NOMPRODUCTO, D.COSTOPROD, D.PRECIO ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.SERVIDO, A.IDZONA, A.NUMMESA, A.IDUSUARIO, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C INNER JOIN RESTAURANTEPRODUCTO D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO ";
		sql += "WHERE C.SERVIDO = '1' AND IDUSUARIO IS NOT NULL ";
		sql += "AND C.NOMRESTAURANTE LIKE '" + nRestaurante + "'";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			Long idZona = rs.getLong("IDZONA");
			Long numMesa = rs.getLong("NUMMESA");
			String idUsuario = rs.getString("IDUSUARIO");
			String nomProducto = rs.getString("NOMPRODUCTO");
			int costoProd = rs.getInt("COSTOPROD");
			int precio = rs.getInt("PRECIO");
			RFC8Registrado nuevo = new RFC8Registrado(idPedido, nomRestaurante, idZona, numMesa, idUsuario, nomProducto, costoProd, precio);
			lista.add(nuevo);
		}
		return lista;
	}
	
	public ArrayList<RFC8NoRegistrado> darConsolidadoUsuariosNoRegistrados() throws SQLException, Exception{
		ArrayList<RFC8NoRegistrado> lista = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.NOMRESTAURANTE, C.IDZONA, C.NUMMESA, C.IDUSUARIO, C.NOMPRODUCTO, D.COSTOPROD, D.PRECIO ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.SERVIDO, A.IDZONA, A.NUMMESA, A.IDUSUARIO, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C INNER JOIN RESTAURANTEPRODUCTO D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO ";
		sql += "WHERE C.SERVIDO = '1'";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			Long idZona = rs.getLong("IDZONA");
			Long numMesa = rs.getLong("NUMMESA");
			String idUsuario = rs.getString("IDUSUARIO");
			String nomProducto = rs.getString("NOMPRODUCTO");
			int costoProd = rs.getInt("COSTOPROD");
			int precio = rs.getInt("PRECIO");
			RFC8NoRegistrado nuevo = new RFC8NoRegistrado(idPedido, nomRestaurante, idZona, numMesa, idUsuario, nomProducto, costoProd, precio);
			lista.add(nuevo);
		}
		return lista;
	}
	
	
	public ArrayList<RFC8NoRegistrado> darConsolidadoUsuariosNoRegistradosRestaurante(String nRestaurante) throws SQLException, Exception{
		ArrayList<RFC8NoRegistrado> lista = new ArrayList<>();
		
		String sql = "SELECT C.IDPEDIDO, C.NOMRESTAURANTE, C.IDZONA, C.NUMMESA, C.IDUSUARIO, C.NOMPRODUCTO, D.COSTOPROD, D.PRECIO ";
		sql += "FROM(";
		sql += "SELECT A.IDPEDIDO, A.SERVIDO, A.IDZONA, A.NUMMESA, A.IDUSUARIO, B.NOMPRODUCTO, B.NOMRESTAURANTE ";
		sql += "FROM PEDIDO A INNER JOIN PRODUCTOPEDIDO B ";
		sql += "ON A.IDPEDIDO = B.IDPEDIDO) C INNER JOIN RESTAURANTEPRODUCTO D ";
		sql += "ON C.NOMPRODUCTO = D.NOMPRODUCTO ";
		sql += "WHERE C.SERVIDO = '1' ";
		sql += "AND C.NOMRESTAURANTE LIKE '" + nRestaurante + "'";
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Long idPedido = rs.getLong("IDPEDIDO");
			String nomRestaurante = rs.getString("NOMRESTAURANTE");
			Long idZona = rs.getLong("IDZONA");
			Long numMesa = rs.getLong("NUMMESA");
			String idUsuario = rs.getString("IDUSUARIO");
			String nomProducto = rs.getString("NOMPRODUCTO");
			int costoProd = rs.getInt("COSTOPROD");
			int precio = rs.getInt("PRECIO");
			RFC8NoRegistrado nuevo = new RFC8NoRegistrado(idPedido, nomRestaurante, idZona, numMesa, idUsuario, nomProducto, costoProd, precio);
			lista.add(nuevo);
		}
		return lista;
	}
	
	
}