package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;

import vos.Pedido;
import vos.Zona;

public class DAOTablaPedidos 
{
	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaProductos
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTablaPedidos() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}

	public ArrayList<Pedido> darPedidos() throws SQLException, Exception {
		ArrayList<Pedido> Pedidoz = new ArrayList<Pedido>();

		String sql = "SELECT * FROM PEDIDO";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int mesa = rs.getInt("NUMMESA");

			Pedidoz.add(new Pedido(idp,f,idz,p,i,boolCancelado,boolServido,mesa));
		}
		return Pedidoz;
	}

	public Pedido darPedidoPorId(Long id) throws SQLException, Exception {

		Pedido Pedidoz=null;

		String sql = "SELECT * FROM PEDIDO WHERE IDPEDIDO= "+id;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int mesa = rs.getInt("NUMMESA");

			Pedidoz=new Pedido(idp,f,idz,p,i,boolCancelado,boolServido, mesa);

		}
		return Pedidoz;
	}

	public ArrayList<Pedido> darPedidoPorIdCliente(String idC) throws SQLException, Exception {
		ArrayList<Pedido> Pedidoz=new ArrayList<Pedido>();

		String sql = "SELECT * FROM PEDIDO WHERE IDUSUARIO= '"+idC+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int mesa = rs.getInt("NUMMESA");

			Pedidoz.add(new Pedido(idp,f,idz,p,i,boolCancelado,boolServido, mesa));

		}
		return Pedidoz;
	}

	public void addPedido(Pedido prod) throws SQLException, Exception {

		Pedido testeo=darPedidoPorId(prod.getIdPedido());
		
		if(testeo!=null)
			throw new Exception ("Un pedido con la id "+prod.getIdPedido()+" ya existe.");

		//		String sql = "INSERT INTO PEDIDO VALUES(SYSDATE,";
		//		sql += prod.getIdPedido()+",";
		//		sql += prod.getIdZona()+",";
		//		sql += 0+", '";
		//		sql += prod.getIdUsuario()+"',";
		//		sql += prod.isServidoCHAR() + ",";
		//		sql += prod.isCanceladoCHAR()+ ",";
		//		sql += prod.getNumMesa()+")";


		String sql ="INSERT INTO PEDIDO VALUES(SYSDATE,?,?,?,?,?,?,?)";

		 PreparedStatement prepStmt = conn.prepareStatement(sql);
		 prepStmt.setLong(1, prod.getIdPedido());
		 prepStmt.setLong(2, prod.getIdZona());
		 prepStmt.setInt(3,0);
		 prepStmt.setString(4, prod.getIdUsuario());
		 prepStmt.setInt(5, prod.isServidoCHAR());
		 prepStmt.setInt(6, prod.isCanceladoCHAR());
		 prepStmt.setInt(7, prod.getNumMesa());
		// recursos.add(prepStmt);
		 prepStmt.executeQuery();

	}
	

	public void addPedidoInd(Pedido prod) throws SQLException, Exception {

		Pedido testeo=darPedidoPorId(prod.getIdPedido());
		if(testeo!=null)
			throw new Exception ("Un pedido con la id "+prod.getIdPedido()+" ya existe.");

		String sql;
		if(prod.getIdUsuario()!=null) {
		 sql = "INSERT INTO PEDIDO VALUES(SYSDATE,";
		sql += prod.getIdPedido()+",";
		sql += prod.getIdZona()+",";
		sql += 0+", '";
		sql += prod.getIdUsuario()+"',";
		sql += "0,";
		sql += "0,";
		sql += prod.getNumMesa()+")";
		}
		else
		{
			 sql = "INSERT INTO PEDIDO VALUES(SYSDATE,";
			sql += prod.getIdPedido()+",";
			sql += prod.getIdZona()+",";
			sql += 0+",";
			sql += "NULL,";
			sql += "0,";
			sql += "0,";
			sql += prod.getNumMesa()+")";
		}

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();

	}

	public void updatePedido(Pedido prod) throws SQLException, Exception {

		String sql = "UPDATE PEDIDO SET PRECIOTOTAL="+prod.getPrecioTotal();
		sql += " WHERE IDPEDIDO = " + prod.getIdPedido()+"";


		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}

	public void deletePedido(Pedido prod) throws SQLException, Exception {

		String sql = "DELETE FROM PEDIDO";
		sql += " WHERE IDPEDIDO = " + prod.getIdPedido();

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}

	public ArrayList<Zona> buscarZonaPedidos() throws SQLException, Exception {

		ArrayList<Pedido> pedidos = darPedidos();
		DAOTablaZonas tz = new DAOTablaZonas();
		ArrayList<Zona> zonas = new ArrayList<Zona>();

		for (int i = 0; i < pedidos.size(); i++) {

			Zona zonitaPedido = tz.buscarZonaPorId(pedidos.get(i).getIdZona());
			zonas.add(zonitaPedido);
		}
		return zonas;
	}

	public Pedido buscarPedidoPorZona(Long idZona) throws SQLException, Exception 

	{
		Pedido pedidoBuscado = null;

		String sql = "SELECT * FROM PEDIDO";
		sql += "WHERE IDZONA = " + idZona;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int mesa = rs.getInt("NUMMESA");

			pedidoBuscado = new Pedido(idp,f,idz,p, i,boolCancelado, boolServido, mesa);
		}

		return pedidoBuscado;
	}

	public ArrayList<Pedido> darPedidosPorIdZona(Long id) throws SQLException, Exception {

		ArrayList<Pedido> Pedidoz = new ArrayList<Pedido>();

		String sql = "SELECT * FROM PEDIDO WHERE IDZONA= "+id;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int mesa = rs.getInt("NUMMESA");

			Pedidoz.add(new Pedido(idp,f,idz,p,i,boolCancelado,boolServido, mesa));
	
		}
		return Pedidoz;
	}

	public void cancelarPedido(Long idPedido) throws SQLException, Exception{
		String sql="UPDATE PEDIDO SET CANCELADO='1'";
		sql+="WHERE IDPEDIDO="+idPedido;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}

	public void servirPedido(Long idPedido) throws SQLException, Exception{
		String sql="UPDATE PEDIDO SET SERVIDO='1'";
		sql+="WHERE IDPEDIDO="+idPedido;

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}

	public ArrayList<Pedido> RF16(int mesa) throws SQLException, Exception {

		ArrayList<Pedido> Pedidoz = new ArrayList<Pedido>();
		String sql1="SET TRANSACTION ISOLATION LEVEL READ COMMITTED"; 
		String sql="UPDATE PEDIDO SET SERVIDO ='1' WHERE SERVIDO = '0' AND NUMMESA =?";
		PreparedStatement prepStmt1 = conn.prepareStatement(sql1);
		prepStmt1.executeQuery();
		conn.commit();
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		prepStmt.setInt(1, mesa);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
		conn.commit();
		Pedidoz = darPedidosPorMesa2(mesa);
	
		return Pedidoz;
	}
	public void RF15(ArrayList<Pedido> pedidosMesa) throws SQLException, Exception {

		String sql = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED";		
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();

		for (int i = 0; i < pedidosMesa.size(); i++) {

			Pedido actual = pedidosMesa.get(i);
			addPedido(actual);
		}
	}
	public ArrayList<Pedido> darPedidosPorMesa(int mesa) throws SQLException, Exception {

		ArrayList<Pedido> Pedidoz = new ArrayList<Pedido>();

		String sql ="SELECT * FROM  PEDIDO WHERE SERVIDO ='0'AND NUMMESA=?";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		prepStmt.setInt(1, mesa);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int me = rs.getInt("NUMMESA");

			Pedidoz.add(new Pedido(idp,f,idz,p,i,boolCancelado,boolServido, me));			
		}
		return Pedidoz;
	}

	public ArrayList<Pedido> darPedidosPorMesa2(int mesa) throws SQLException, Exception {

		ArrayList<Pedido> Pedidoz = new ArrayList<Pedido>();

		String sql ="SELECT * FROM  PEDIDO WHERE NUMMESA=?";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		prepStmt.setInt(1, mesa);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			Long idp=rs.getLong("IDPEDIDO");
			Date f=rs.getDate("FECHA");
			Long idz=rs.getLong("IDZONA");
			double p=rs.getDouble("PRECIOTOTAL");
			String i=rs.getString("IDUSUARIO");
			String servido = rs.getString("SERVIDO");
			String cancelado = rs.getString("CANCELADO");
			boolean boolServido = servido.equals("1");
			boolean boolCancelado = cancelado.equals("1");
			int me = rs.getInt("NUMMESA");

			Pedidoz.add(new Pedido(idp,f,idz,p,i,boolCancelado,boolServido, me));			
		}
		return Pedidoz;
	}

}
