package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC9Simple {
	
	@JsonProperty("idUsuario")
	private String idUsuario;
	
	@JsonProperty("cantidadPedidos")
	private int cantidadPedidos;
	
	@JsonProperty("nombre")
	private String nombre;
	
	@JsonProperty("correo")
	private String correo;
	
	@JsonProperty("rol")
	private String rol;
	
	public RFC9Simple(@JsonProperty("idUsuario") String idUsuario, @JsonProperty("cantidadPedidos") int cantidadPedidos,
			@JsonProperty("nombre") String nombre, @JsonProperty("correo") String correo, @JsonProperty("rol") String rol){
		this.idUsuario = idUsuario;
		this.cantidadPedidos = cantidadPedidos;
		this.nombre = nombre;
		this.correo = correo;
		this.rol = rol;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getCantidadPedidos() {
		return cantidadPedidos;
	}

	public void setCantidadPedidos(int cantidadPedidos) {
		this.cantidadPedidos = cantidadPedidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
	

}