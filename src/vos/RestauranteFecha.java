package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RestauranteFecha {
	
	@JsonProperty("restaurante")
	private String restaurante;
	
	@JsonProperty("fechaInicial")
	private String fechaInicial;
	
	@JsonProperty("fechaFinal")
	private String fechaFinal;
	
	public RestauranteFecha(@JsonProperty("restaurante") String restaurante, @JsonProperty("fechaInicial") String fechaInicial,
			@JsonProperty("fechaFinal") String fechaFinal){
		this.restaurante = restaurante;
		this.fechaFinal = fechaFinal;
		this.fechaInicial = fechaInicial;
	}

	public String getRestaurante() {
		return restaurante;
	}

	public void setRestaurante(String restaurante) {
		this.restaurante = restaurante;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	
}
