package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC7 {
	
	@JsonProperty("idPedido")
	private Long idPedido;
	
	@JsonProperty("idUsuario")
	private String idUsuario;
	
	@JsonProperty("numMesa")
	private Long numMesa;
	
	@JsonProperty("nomProducto")
	private String nomProducto;
	
	@JsonProperty("nomRestaurante")
	private String nomRestaurante;
	
	@JsonProperty("nomMenu")
	private String nomMenu;
	
	public RFC7(@JsonProperty("idPedido") Long idPedido, @JsonProperty("idUsuario") String idUsuario,
			@JsonProperty("numMesa") Long numMesa, @JsonProperty("nomProducto") String nomProducto,
			@JsonProperty("nomRestaurante") String nomRestaurante, @JsonProperty("nomMenu") String nomMenu){
		this.idPedido = idPedido;
		this.idUsuario = idUsuario;
		this.numMesa = numMesa;
		this.nomProducto = nomProducto;
		this.nomRestaurante = nomRestaurante;
		this.nomMenu = nomMenu;
	}

	public Long getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getNumMesa() {
		return numMesa;
	}

	public void setNumMesa(Long numMesa) {
		this.numMesa = numMesa;
	}

	public String getNomProducto() {
		return nomProducto;
	}

	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}

	public String getNomRestaurante() {
		return nomRestaurante;
	}

	public void setNomRestaurante(String nomRestaurante) {
		this.nomRestaurante = nomRestaurante;
	}

	public String getNomMenu() {
		return nomMenu;
	}

	public void setNomMenu(String nomMenu) {
		this.nomMenu = nomMenu;
	}
	
	
	

}