package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC11FechaProducto {

	@JsonProperty("fecha")
	private String fecha;
	
	@JsonProperty("cantidad")
	private int cantidad;
	
	@JsonProperty("nomProducto")
	private String nomProducto;
	
	
	public RFC11FechaProducto(@JsonProperty("fecha")String fecha, @JsonProperty("cantidad") int cantidad, 
			@JsonProperty("nomProducto") String nomProducto){
		this.cantidad = cantidad;
		this.nomProducto = nomProducto;
		this.fecha = fecha;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getNomProducto() {
		return nomProducto;
	}

	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}
}
