/**-------------------------------------------------------------------
 * $Id$
 * Universidad de los Andes (Bogotá - Colombia)
 * Departamento de Ingeniería de Sistemas y Computación
 *
 * Materia: Sistemas Transaccionales
 * Ejercicio: RestauranteProductoAndes
 * Autor: Juan Felipe García - jf.garcia268@uniandes.edu.co
 * -------------------------------------------------------------------
 */
package vos;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Clase que representa una arreglo de RestauranteProducto
 * @author Juan
 */
public class ListaRestauranteProducto {
	
	/**
	 * List con los RestauranteProductos
	 */
	@JsonProperty(value="RestauranteProductos")
	private List<RestauranteProducto> RestauranteProductos;
	
	/**
	 * Constructor de la clase ListaRestauranteProductos
	 * @param RestauranteProductos - RestauranteProductos para agregar al arreglo de la clase
	 */
	public ListaRestauranteProducto( @JsonProperty(value="RestauranteProductos")List<RestauranteProducto> RestauranteProductos){
		this.RestauranteProductos = RestauranteProductos;
	}

	/**
	 * Método que retorna la lista de RestauranteProductos
	 * @return  List - List con los RestauranteProductos
	 */
	public List<RestauranteProducto> getRestauranteProductos() {
		return RestauranteProductos;
	}

	/**
	 * Método que asigna la lista de RestauranteProductos que entra como parametro
	 * @param  RestauranteProductos - List con los RestauranteProductos ha agregar
	 */
	public void setRestauranteProducto(List<RestauranteProducto> RestauranteProductos) {
		this.RestauranteProductos = RestauranteProductos;
	}
	
}
