package vos;

import java.sql.Date;

import org.codehaus.jackson.annotate.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Pedido {
	
	@JsonProperty(value="idPedido")
	private Long idPedido;
	
	@JsonProperty(value="fecha")
	private String fecha;
	
	@JsonProperty(value="idZona")
	private Long idZona;
	
	@JsonProperty(value="precioTotal")
	private double precioTotal;
	
	@JsonProperty(value="idUsuario")
	private String idUsuario;
	
	@JsonProperty(value="cancelado")
	private boolean cancelado;
	
	@JsonProperty(value="servido")
	private boolean servido;
	
	@JsonProperty(value="numMesa")

	private int numMesa;
	
	private ArrayList<EquivalenciaProducto> equivalenciasProductos;
	
	private ArrayList<EquivalenciaIngrediente> equivalenciasIngredientes;
		
	public Pedido(@JsonProperty(value="idPedido") Long idP, @JsonProperty(value="fecha") Date f, @JsonProperty(value="idZona") Long idZ, 
			@JsonProperty(value="precioTotal") double pre, @JsonProperty(value="idUsuario") String idUsuario, @JsonProperty(value="cancelado") boolean cancelado,

			@JsonProperty(value="servido") boolean servido, @JsonProperty(value="numMesa") int mesa)

	{
		this.idPedido=idP;
		
		SimpleDateFormat ft =new SimpleDateFormat ("yyyy/MM/dd");
		this.fecha=ft.format(f);
		
				
		this.idZona=idZ;
		
		this.precioTotal=pre;
		
		if(idUsuario==null||idUsuario.isEmpty())
			this.idUsuario=null;
		else
			this.idUsuario=idUsuario;
		
		this.cancelado = cancelado;
		
		this.servido = servido;

		this.numMesa = mesa;

	}
	

	public Long getIdPedido() {
		return idPedido;
	}


	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}



	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public Long getIdZona() {
		return idZona;
	}

	public void setIdZona(Long idZona) {
		this.idZona = idZona;
	}



	public double getPrecioTotal() {
		return precioTotal;
	}



	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}


	public String getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	public boolean isCancelado() {
		return cancelado;
	}


	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}


	public boolean isServido() {
		return servido;
	}


	public void setServido(boolean servido) {
		this.servido = servido;
	}
	
	
	public int isServidoCHAR(){
		if(servido){
			return 1;
		}else{
			return 0;
		}
	}
	
	public int isCanceladoCHAR(){
		if(cancelado){
			return 1;
		}else{
			return 0;
		}
	}	

	public ArrayList<EquivalenciaProducto> getEquivalenciasProductos() {
		return equivalenciasProductos;
	}

	public int getNumMesa() {
		return numMesa;
	}


	public void setNumMesa(int numMesa) {
		this.numMesa = numMesa;
	}	

	public void setEquivalenciasProductos(ArrayList<EquivalenciaProducto> equivalenciasProductos) {
		this.equivalenciasProductos = equivalenciasProductos;
	}


	public ArrayList<EquivalenciaIngrediente> getEquivalenciasIngredientes() {
		return equivalenciasIngredientes;
	}


	public void setEquivalenciasIngredientes(ArrayList<EquivalenciaIngrediente> equivalenciasIngredientes) {
		this.equivalenciasIngredientes = equivalenciasIngredientes;
	}

}