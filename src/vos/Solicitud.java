package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class Solicitud {
	
	@JsonProperty(value="idUsuario")
	private String idUsuario;
	
	@JsonProperty(value="estado")
	private String estado;
	
	@JsonProperty(value="idPedido")
	private int idPedido;
	
	public Solicitud(@JsonProperty(value="idUsuario") String idUs, @JsonProperty(value="estado") String es, @JsonProperty(value="idPedido") int idP)
	{
		super();
		this.idUsuario=idUs;
		this.estado=es;
		this.idPedido=idP;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	
	
	

}
