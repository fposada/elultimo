/**-------------------------------------------------------------------
 * $Id$
 * Universidad de los Andes (Bogotá - Colombia)
 * Departamento de Ingeniería de Sistemas y Computación
 *
 * Materia: Sistemas Transaccionales
 * Ejercicio: RestauranteAndes
 * Autor: Juan Felipe García - jf.garcia268@uniandes.edu.co
 * -------------------------------------------------------------------
 */
package vos;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Clase que representa una arreglo de Restaurante
 * @author Juan
 */
public class ListaRestaurantes {
	
	/**
	 * List con los Restaurantes
	 */
	@JsonProperty(value="Restaurantes")
	private List<Restaurante> Restaurantes;
	
	/**
	 * Constructor de la clase ListaRestaurantes
	 * @param Restaurantes - Restaurantes para agregar al arreglo de la clase
	 */
	public ListaRestaurantes( @JsonProperty(value="Restaurantes")List<Restaurante> Restaurantes){
		this.Restaurantes = Restaurantes;
	}

	/**
	 * Método que retorna la lista de Restaurantes
	 * @return  List - List con los Restaurantes
	 */
	public List<Restaurante> getRestaurantes() {
		return Restaurantes;
	}

	/**
	 * Método que asigna la lista de Restaurantes que entra como parametro
	 * @param  Restaurantes - List con los Restaurantes ha agregar
	 */
	public void setRestaurante(List<Restaurante> Restaurantes) {
		this.Restaurantes = Restaurantes;
	}
	
}
