/**-------------------------------------------------------------------
 * $Id$
 * Universidad de los Andes (Bogotá - Colombia)
 * Departamento de Ingeniería de Sistemas y Computación
 *
 * Materia: Sistemas Transaccionales
 * Ejercicio: ProductoAndes
 * Autor: Juan Felipe García - jf.garcia268@uniandes.edu.co
 * -------------------------------------------------------------------
 */
package vos;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Clase que representa una arreglo de Producto
 * @author Juan
 */
public class ListaProductos {
	
	/**
	 * List con los Productos
	 */
	@JsonProperty(value="Productos")
	private List<Producto> Productos;
	
	/**
	 * Constructor de la clase ListaProductos
	 * @param Productos - Productos para agregar al arreglo de la clase
	 */
	public ListaProductos( @JsonProperty(value="Productos")List<Producto> Productos){
		this.Productos = Productos;
	}

	/**
	 * Método que retorna la lista de Productos
	 * @return  List - List con los Productos
	 */
	public List<Producto> getProductos() {
		return Productos;
	}

	/**
	 * Método que asigna la lista de Productos que entra como parametro
	 * @param  Productos - List con los Productos ha agregar
	 */
	public void setProducto(List<Producto> Productos) {
		this.Productos = Productos;
	}
	
}
