package rest;

import javax.print.attribute.standard.MediaPrintableArea;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.media.jfxmedia.Media;

import tm.RotondAndesTM;
import vos.RFC8Consolidador;

@Path("resultadosVentas/{idUsuario}")
public class RotondAndesRFC8 {
	/**
	 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
	 */
	@Context
	private ServletContext context;

	/**
	 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
	 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
	 */
	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}


	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response darResultados(@PathParam("idUsuario") String idUsuario){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			RFC8Consolidador respuesta = tm.consolidarPedidos(idUsuario);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@GET
	@Path("/{nomRestaurante}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response darResultadosRestaurante(@PathParam("idUsuario") String idUsuario, 
			@PathParam("nomRestaurante") String nomRestaurante){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			RFC8Consolidador respuesta = tm.consolidarPedidos(idUsuario, nomRestaurante);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
}