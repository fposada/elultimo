package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.RotondAndesTM;
import vos.RFC9Simple;
import vos.RestauranteFecha;

@Path("RFC9/{idUsuario}")
public class RotondAndesRFC9 {
	
	/**
	 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
	 */
	@Context
	private ServletContext context;

	/**
	 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
	 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
	 */
	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}


	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response darConsumoUsuarios(@PathParam("idUsuario")String idUsuario, RestauranteFecha rf){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC9Simple> respuesta = tm.darConsumoUsuarios(idUsuario, rf, "");
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/ordenUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	public Response darConsumoUsuariosOrdenadoUsuario(@PathParam("idUsuario")String idUsuario, RestauranteFecha rf){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC9Simple> respuesta = tm.darConsumoUsuarios(idUsuario, rf, "ordenUsuario");
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/ordenProducto")
	@Produces(MediaType.APPLICATION_JSON)
	public Response darConsumoUsuariosOrdenadoProducto(@PathParam("idUsuario")String idUsuario, RestauranteFecha rf){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC9Simple> respuesta = tm.darConsumoUsuarios(idUsuario, rf, "ordenProducto");
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/ordenTipo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response darConsumoUsuariosOrdenadoTipoProducto(@PathParam("idUsuario")String idUsuario, RestauranteFecha rf){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC9Simple> respuesta = tm.darConsumoUsuarios(idUsuario, rf, "ordenTipo");
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}

}