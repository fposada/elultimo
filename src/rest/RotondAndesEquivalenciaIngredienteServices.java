package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import tm.RotondAndesTM;
import vos.EquivalenciaIngrediente;
import vos.EquivalenciaIngredientePedido;


@Path("{idUsuario}/EquivalenciaIngredientes")
public class RotondAndesEquivalenciaIngredienteServices {

	/**
	 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
	 */
	@Context
	private ServletContext context;

	/**
	 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
	 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
	 */
	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}


	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getEquivalenciaIngredientes() {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		List<EquivalenciaIngrediente> EquivalenciaIngredientes;
		try {
			EquivalenciaIngredientes = tm.darEquivalenciasIngrediente();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaIngredientes).build();
	}

	@GET
	@Path( "{nameI}/{nameR}" )
	@Produces( { MediaType.APPLICATION_JSON } )
	public Response getEquivalenciaIngrediente( @PathParam( "nameI" ) String nameI, @PathParam("nameR") String nameR )
	{
		List<EquivalenciaIngrediente> v;
		RotondAndesTM tm = new RotondAndesTM( getPath( ) );
		try
		{
			v = tm.darEquivalenciasIngredientePorIngredienteEnRestaurante(nameI, nameR);
			return Response.status( 200 ).entity( v ).build( );			
		}
		catch( Exception e )
		{
			return Response.status( 500 ).entity( doErrorMessage( e ) ).build( );
		}
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEquivalenciaIngrediente(@PathParam("idUsuario") String idUsuario,EquivalenciaIngrediente EquivalenciaIngrediente) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.addEquivalenciaIngrediente(idUsuario,EquivalenciaIngrediente);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaIngrediente).build();
	}
	
	@POST
	@Path("/enPedido")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEquivalenciaIngredientePedido(@PathParam("idUsuario") String idUsuario,EquivalenciaIngredientePedido EquivalenciaIngrediente) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.addEquivalenciaIngredientePedido(idUsuario,EquivalenciaIngrediente);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaIngrediente).build();
	}


//	@PUT
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response updateEquivalenciaIngrediente(@PathParam("idUsuario") String idUsuario,EquivalenciaIngrediente EquivalenciaIngrediente) {
//		RotondAndesTM tm = new RotondAndesTM(getPath());
//		try {
//			tm.updateEquivalenciaIngrediente(idUsuario,EquivalenciaIngrediente);
//		} catch (Exception e) {
//			return Response.status(500).entity(doErrorMessage(e)).build();
//		}
//		return Response.status(200).entity(EquivalenciaIngrediente).build();
//	}


	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteEquivalenciaIngrediente(@PathParam("idUsuario") String idUsuario,EquivalenciaIngrediente EquivalenciaIngrediente) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.deleteEquivalenciaIngrediente(idUsuario,EquivalenciaIngrediente);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaIngrediente).build();
	}
}


