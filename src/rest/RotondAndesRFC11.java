package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.RotondAndesTM;
import vos.FechaInicial;
import vos.RFC11FechaProducto;
import vos.RFC11FechaRestaurante;

@Path("RFC11/{idUsuario}")
public class RotondAndesRFC11 {

	/**
	 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
	 */
	@Context
	private ServletContext context;

	/**
	 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
	 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
	 */
	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}


	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}
	
	@PUT
	@Path("/producto/MAS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductoMASConsumido(@PathParam("idUsuario") String idUsuario, FechaInicial fi){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC11FechaProducto> respuesta = tm.darProductoMasConsumido(idUsuario, fi);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/producto/MENOS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductoMENOSConsumido(@PathParam("idUsuario") String idUsuario, FechaInicial fi){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC11FechaProducto> respuesta = tm.darProductoMenosConsumido(idUsuario, fi);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/restaurante/MAS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRestauranteMASFrecuentado(@PathParam("idUsuario") String idUsuario, FechaInicial fi){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC11FechaRestaurante> respuesta = tm.darRestauranteMasFrecuentado(idUsuario, fi);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
	
	@PUT
	@Path("/restaurante/MENOS")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRestauranteMENOSFrecuentado(@PathParam("idUsuario") String idUsuario, FechaInicial fi){
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try{
			List<RFC11FechaRestaurante> respuesta = tm.darRestauranteMenosFrecuentado(idUsuario, fi);
			return Response.status(200).entity(respuesta).build();
		}catch(Exception e){
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
	}
}